---
title: linux服务器内存查看
date: 2022-02-10 15:49:50
categories:
- Linux
tags:
- Linux
index_img:
banner_img:
banner_img_height:
banner_mask_alpha:
---

查看linux服务器内存使用情况 

<!-- more -->

## 1. free命令

free 命令显示系统使用和空闲的内存情况，包括物理内存、交互区内存(swap)和内核缓冲区内存。

```fr
# 自动选择以适合理解的容量单位显示
free -h

# free命令默认是显示单位kb，可以采用free -m和free -g命令查看，分别表示MB和GB
free
free -m
free -g
```

![free截图](https://s2.loli.net/2022/02/10/PZf5O3jFHw8YEnQ.png)

注解：
Mem:表示物理内存统计，如果机器剩余内存非常小，一般小于总内存的20%，则判断为系统物理内存不够。
Swap: 表示硬盘上交换分区的使用情况，如剩余空间较小，需要留意当前系统内存使用情况及负载，当Swap的used值大于0时，则表示操作系统物理内存不够，已经开始使用硬盘内存了。

> total: 972M表示物理内存总量；
>
> used: 462M表示总计分配给缓存(包含buffers与cache)使用的数量，但其中可能部分缓存并未实际使用；
>
> free：1.3G表示未被分配的内存；
>
> shared: 表示共享内存；
>
> buff/cache: 435M表示系统分配但未被使用的buffers数量；
>
> available: 341M表示系统分配但未被使用的available数量；

### 排查问题

```
ps -eo pmem,pcpu,rss,vsize,args | sort -k 1 -r | less
```

![执行截图](https://s2.loli.net/2022/02/10/98MHZ3tjeFRUA6i.png)



## 2.top命令

top 命令查看系统的实时负载， 包括进程、CPU负载、内存使用等等；

![top截图](https://s2.loli.net/2022/02/10/LO5l8XcuokTCPKF.png)

%Cpu(s)

>us	用户空间占用CPU百分比
>
>sy	内核空间占用CPU百分比
>
>ni	用户进程空内改变过优先级的进程占用CPU百分比
>
>id	空闲CPU百分比
>
>wa	等待输入输出的CPU时间百分比
>
>hi	CPU服务于硬件中断所耗费的时间总额
>
>si	CPU服务软中断所耗费的时间总额
>
>st	Steal Time

列表的含义

>PID	进程ID
>
>USER	进程所有者
>
>PR	优先级
>
>NI	nice值，负值表示高优先级，正值表示低优先级
>
>VIRT	进程使用的虚拟内存总量
>
>RES	进程使用的、未被换出的物理内存大小
>
>SHR	共享内存大小
>
>S	进程状态
>
>%CPU	上次更新到现在的CPU时间占用百分比
>
>%MEM	进程使用的物理内存百分比
>
>TIME+	进程使用CPU总时间
>
>COMMAND	命令名、命令行

进入top的实时界面后，默认按照CPU的使用率排序，通过“shift+m”按键将进程按照内存使用情况排序，可以查看哪些进程是当前系统中的内存开销“大户”。

top命令中，按下 f 键，进入选择排序列的界面，这里可以选择要显示的信息列，要按照哪些信息列进行排序。
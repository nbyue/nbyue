---
title: max-binlog-cache-size处理与优化
date: 2022-01-29 16:08:18
categories:
- [MySQL, 异常处理]
tags: 
- MySQL
index_img: /post_img/mysql-error-1197.jpg
banner_img: 
banner_img_height: 
banner_mask_alpha: 
---

```bash
ERROR 1197 (HY000) at line 1: Multi-statement transaction required more than 'max_binlog_cache_size' bytes of storage; increase this mysqld variable and try again。
```

<!-- more -->

## 参数解释

binlog cache：它是用于缓存binlog event的内存，大小由binlog_cache_size控制；

binlog_cache_size：为每个session 分配的内存，用于更新二进制日志的事务引擎的事务缓存的大小。如果您经常使用包含许多语句的事务，则可以增加此语句以获得更高的性能；

binlog cache 临时文件：是一个临时磁盘文件，存储由于binlog cache不足溢出的binlog event，该文件名字由"ML"打头，由参数max_binlog_cache_size控制该文件大小；

max_binlog_cache_size：表示的是binlog 能够使用的最大cache 内存大小；

Binlog_cache_disk_use表示因为我们binlog_cache_size设计的内存不足导致缓存二进制日志用到了临时文件的次数；Binlog_cache_use 表示用binlog_cache_size缓存的次数，当对应的Binlog_cache_disk_use 值比较大的时候 我们可以考虑适当的调高 binlog_cache_size 对应的值；

binlog file：代表binglog 文件，由max_binlog_size指定大；

binlog event：代表binlog中的记录，如MAP_EVENT/QUERY EVENT/XID EVENT/WRITE EVENT等；

### 事务binlog event写入流程

binlog cache和binlog临时文件都是在事务运行过程中写入，一旦事务提交，binlog cache和binlog临时文件都会释放掉。而且如果事务中包含多个DML语句，他们共享binlog cache和binlog 临时文件。

整个binlog写入流程如下：

1. 事务开启

2. 执行dml语句，在dml语句第一次执行的时候会分配内存空间binlog cache

3. 执行dml语句期间生成的event不断写入到binlog cache

4. 如果binlog cache的空间已经满了，则将binlog cache的数据写入到binlog临时文件，同时清空binlog cache。如果binlog临时文件的大小大于了max_binlog_cache_size的设置则抛错ERROR 1197

5. 事务提交，整个binlog cache和binlog临时文件数据全部写入到binlog file中，同时释放binlog cache和binlog临时文件。但是注意此时binlog cache的内存空间会被保留以供THD上的下一个事务使用，但是binlog临时文件被截断为0，保留文件描述符。其实也就是IO_CACHE(参考后文)保留，并且保留IO_CACHE中的分配的内存空间，和物理文件描述符

6. 客户端断开连接，这个过程会释放IO_CACHE同时释放其持有的binlog cache内存空间以及持有的binlog 临时文件。


## 故障复盘

1. 表行值太大，大概500M，每次删除5000条，出现此错误。
2. load data，数据太大，出现此错误



## 参考

[^1]: https://blog.csdn.net/weixin_39811101/article/details/113431781?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1.pc_relevant_default&utm_relevant_index=1